package com.example.bank_account.file

import com.example.bank_account.dto.entity.AccountDto
import com.example.bank_account.dto.request.TransactionDto
import com.example.bank_account.entity.AccountEntity
import com.example.bank_account.entity.TransactionEntity


fun toAccountEntity(accountDto: AccountDto): AccountEntity {
    return AccountEntity(
            id = accountDto.id,
            customerId = accountDto.customerId,
            amount = accountDto.amount,
            currency = accountDto.currency
    )
}

fun toAccountDto(accountEntity: AccountEntity): AccountDto {
    return AccountDto(
        id = accountEntity.id,
        customerId = accountEntity.customerId,
        amount = accountEntity.amount,
        currency = accountEntity.currency
    )
}

fun toTransactionEntity(dto: TransactionDto) : TransactionEntity{
    return TransactionEntity(
        id = null,
        customerId = dto.customerId,
        amount = dto.amount,
        currency = dto.currency,
        direction = dto.direction,
        description = dto.description ?: "",
        createdAt = null
    )
}

fun toTransactionDto(entity: TransactionEntity) : TransactionDto{
    return TransactionDto(
        customerId = entity.customerId,
        amount = entity.amount,
        currency = entity.currency,
        direction = entity.direction,
        description = entity.description ?: ""
    )
}
