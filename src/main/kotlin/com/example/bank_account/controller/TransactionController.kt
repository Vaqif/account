package com.example.bank_account.controller

import com.example.bank_account.dto.request.TransactionDto
import com.example.bank_account.service.TransactionService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/transactions")
class TransactionController(private val transactionService: TransactionService) {

    @PostMapping
    fun doTransaction( @RequestBody request: TransactionDto): ResponseEntity<Long> {
        return ResponseEntity.ok()
                                .body(transactionService.doTransaction(request))
    }

    @GetMapping("customer/{customerId}")
    fun getTransactions( @PathVariable customerId: Long ) : ResponseEntity<List<TransactionDto>>{
        return ResponseEntity.ok()
                                .body(transactionService.getTransactionsByCustomerId(customerId))
    }
}