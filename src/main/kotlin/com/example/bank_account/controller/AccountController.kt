package com.example.bank_account.controller

import com.example.bank_account.dto.entity.AccountDto
import com.example.bank_account.dto.request.AccountRequest
import com.example.bank_account.service.AccountService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/accounts")
class AccountController (val accountService: AccountService) {

    @PostMapping
    fun createAccount( @RequestBody request: AccountRequest) : ResponseEntity<Any>{
        accountService.createAccount(request)
        return ResponseEntity.status(201).build()
    }

    @GetMapping("/{id}")
    fun getAccountById(@PathVariable id: Long): ResponseEntity<AccountDto> {
        return ResponseEntity.ok()
            .body(accountService.getAccountByIdOrThrowException(id))
    }

    @GetMapping("/customer/{id}")
    fun getAccountsByCustomerId(@PathVariable("id") customerId: Long): ResponseEntity<List<AccountDto>> {
        return ResponseEntity.ok()
            .body(accountService.getAccountsByCustomerId(customerId))
    }

}