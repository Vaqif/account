package com.example.bank_account.enums

enum class Currency {
    AZN, USD, EUR, RUB
}
