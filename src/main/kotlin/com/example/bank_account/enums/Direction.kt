package com.example.bank_account.enums

enum class Direction {
    IN,OUT;

    fun isIn(): Boolean =  this == IN
}
