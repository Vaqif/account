package com.example.bank_account.repository

import com.example.bank_account.dto.entity.AccountDto
import com.example.bank_account.entity.AccountEntity
import com.example.bank_account.enums.Currency
import org.springframework.data.jpa.repository.JpaRepository

interface AccountRepository : JpaRepository<AccountEntity,Long> {
    fun findAllByCustomerId(accountId: Long) : List<AccountEntity>
    fun findAllByCustomerIdAndCurrency(accountId: Long, currency: Currency) : AccountEntity?
    fun findAllById(id: Long) : AccountEntity?
}
