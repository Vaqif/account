package com.example.bank_account.repository

import com.example.bank_account.entity.TransactionEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionRepository : JpaRepository<TransactionEntity,Long> {
    fun findAllByCustomerId(customerId: Long) : List<TransactionEntity>
}
