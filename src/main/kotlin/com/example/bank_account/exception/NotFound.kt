package com.example.bank_account.exception

import java.lang.RuntimeException

class NotFound(private val subject: String) : RuntimeException() {
    override val message: String
        get() = "$subject not found"
}
