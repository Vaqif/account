package com.example.bank_account.exception

import com.fasterxml.jackson.databind.JsonMappingException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.Exception
import java.util.*

@RestControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler
    fun handle(exception: Exception): ResponseEntity<ErrorResponse> {
        return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(ErrorResponse(exception.message ?: ""))
    }

    @ExceptionHandler
    fun handle(exception: NotFound): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body(ErrorResponse(exception.message ?: ""))
    }

    @ExceptionHandler
    fun handle(exception: BadRequest): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(ErrorResponse(exception.message ?: ""))
    }

    @ExceptionHandler
    fun handle(exception: HttpMessageNotReadableException): ResponseEntity<ErrorResponse> {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .build()
    }
}