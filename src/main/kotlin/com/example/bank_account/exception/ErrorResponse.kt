package com.example.bank_account.exception

data class ErrorResponse (val message: String)