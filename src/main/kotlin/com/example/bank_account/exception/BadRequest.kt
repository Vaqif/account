package com.example.bank_account.exception

class BadRequest(override val message: String?) : RuntimeException()
