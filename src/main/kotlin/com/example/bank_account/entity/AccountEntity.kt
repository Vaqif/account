package com.example.bank_account.entity

import com.example.bank_account.enums.Currency
import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "accounts")
data class AccountEntity (
    @Id
    @GeneratedValue
    val id: Long?,
    val customerId: Long,
    val amount: BigDecimal,
    @Enumerated(EnumType.STRING)
    val currency: Currency
)