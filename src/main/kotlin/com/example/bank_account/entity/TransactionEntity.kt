package com.example.bank_account.entity

import com.example.bank_account.enums.Currency
import com.example.bank_account.enums.Direction
import org.hibernate.annotations.CreationTimestamp
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "transactions")
data class TransactionEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    @Column(name = "account_id")
    val customerId: Long,
    val amount: BigDecimal,
    @Enumerated(EnumType.STRING)
    val currency: Currency,
    @Enumerated(EnumType.STRING)
    val direction: Direction,
    val description: String,
    @Column(name = "created_at")
    @CreationTimestamp
    val createdAt: LocalDateTime?
)
