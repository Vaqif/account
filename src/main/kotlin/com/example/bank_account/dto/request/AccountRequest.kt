package com.example.bank_account.dto.request

import com.example.bank_account.enums.Currency

data class AccountRequest(
        val customerId: Long,
        val country: String,
        val currencies: List<Currency>
)
