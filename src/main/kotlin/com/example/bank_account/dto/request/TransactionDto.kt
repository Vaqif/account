package com.example.bank_account.dto.request

import com.example.bank_account.enums.Currency
import com.example.bank_account.enums.Direction
import java.math.BigDecimal

data class TransactionDto (
    val customerId: Long,
    val amount: BigDecimal,
    val currency: Currency,
    val direction: Direction,
    val description: String?,
)
