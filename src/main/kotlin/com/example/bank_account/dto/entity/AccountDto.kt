package com.example.bank_account.dto.entity

import com.example.bank_account.enums.Currency
import com.example.bank_account.exception.BadRequest
import java.math.BigDecimal

data class AccountDto(
    val id: Long?,
    val customerId: Long,
    var amount: BigDecimal,
    val currency: Currency
){
    fun validateOnAvailability(amount: BigDecimal) {
        if (amount > this.amount) throw BadRequest("Not sufficient money")
    }

    companion object{
        fun instance(customerId: Long, currency: Currency) : AccountDto {
            return AccountDto(null, customerId, BigDecimal.ZERO, currency)
        }
    }
}
