package com.example.bank_account.dto

import com.example.bank_account.dto.entity.AccountDto
import com.example.bank_account.enums.Currency

data class AccountResponse (
    val customerId: Long,
    val country: String,
    val accounts: List<AccountDto>
    )
