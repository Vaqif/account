package com.example.bank_account.service

import com.example.bank_account.dto.request.TransactionDto
import com.example.bank_account.file.toTransactionDto
import com.example.bank_account.file.toTransactionEntity
import com.example.bank_account.repository.TransactionRepository
import org.springframework.stereotype.Service

@Service
class TransactionService(
    private val transactionRepository: TransactionRepository,
    private val accountService: AccountService,
    private val publisherService: PublisherService
) {
    fun doTransaction(request: TransactionDto): Long {
        val account = accountService.getAccountByCustomerIdAndCurrencyOrThrowException(request.customerId, request.currency)
        account.validateOnAvailability(request.amount)
        val transactionEntity = transactionRepository.save(toTransactionEntity(request))
        accountService.updateBalance(
                    direction = request.direction,
                    amount = request.amount,
                    accountDto = account
        )
        publisherService.publishTransaction( toTransactionDto(transactionEntity) )
        return transactionEntity.id!!
    }

    fun getTransactionsByCustomerId(customerId: Long): List<TransactionDto> {
        return transactionRepository.findAllByCustomerId(customerId).map { toTransactionDto(it) }
    }
}
