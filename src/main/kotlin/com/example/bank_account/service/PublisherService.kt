package com.example.bank_account.service

import com.example.bank_account.config.RabbitMqConfig
import com.example.bank_account.dto.request.TransactionDto
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service

@Service
class PublisherService(
    private val rabbitTemplate: RabbitTemplate,
    private val rabbitMqConfig: RabbitMqConfig
) {
    fun publishTransaction(transactionDto: TransactionDto){
        rabbitTemplate.convertAndSend(rabbitMqConfig.exchange, rabbitMqConfig.routingKey, transactionDto)
    }
}