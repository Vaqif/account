package com.example.bank_account.service

import com.example.bank_account.dto.AccountResponse
import com.example.bank_account.dto.entity.AccountDto
import com.example.bank_account.dto.request.AccountRequest
import com.example.bank_account.enums.Currency
import com.example.bank_account.enums.Direction
import com.example.bank_account.exception.NotFound
import com.example.bank_account.file.toAccountDto
import com.example.bank_account.file.toAccountEntity
import com.example.bank_account.repository.AccountRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import kotlin.collections.ArrayList

@Service
class AccountService(private val accountRepository: AccountRepository) {

    fun createAccount(request: AccountRequest) {
        accountRepository.saveAll(
            generateAccounts(request).map { toAccountEntity(it) }
        )
        generateResponse(
            accountRepository.findAllByCustomerId( request.customerId )
                         .map { toAccountDto(it) },
            request
        )
    }

    fun getAccountByIdOrThrowException(id: Long): AccountDto {
        return toAccountDto( accountRepository.findAllById(id) ?: throw NotFound("Account"))
    }

    fun getAccountByCustomerIdAndCurrencyOrThrowException(customerId: Long, currency: Currency): AccountDto {
        return toAccountDto(
            accountRepository.findAllByCustomerIdAndCurrency(customerId,currency)
                                                        ?: throw NotFound("Account")
        )
    }

    private fun generateAccounts(request: AccountRequest) : List<AccountDto> {
        val accounts: MutableList<AccountDto> = ArrayList()
        request.currencies
                .forEach {
                    accounts.add(
                                    AccountDto.instance(
                                            customerId = request.customerId,
                                            currency = it)
                    )
                }
        return accounts
    }

    private fun generateResponse(accounts: List<AccountDto>, request: AccountRequest): AccountResponse {
        return AccountResponse(
            customerId = request.customerId,
            country = request.country,
            accounts = accounts
        )
    }

    fun updateBalance(direction: Direction, amount: BigDecimal, accountDto: AccountDto) {
        if (direction.isIn()){
            accountDto.amount = accountDto.amount.plus(amount)
        }else{
            accountDto.amount = accountDto.amount.minus(amount)
        }
        accountRepository.save(toAccountEntity(accountDto))
    }

    fun getAccountsByCustomerId(customerId: Long): List<AccountDto> {
        return accountRepository.findAllByCustomerId(customerId)
            .map { toAccountDto(it) }
    }
}
